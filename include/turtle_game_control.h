#ifndef TURTLE_GAME_CONTROL_H
#define TURTLE_GAME_CONTROL_H
#include "turtle_board_control.h"
#include <map>
#include <string>

class TurtleGameControl{
public:
  TurtleGameControl();
  virtual ~TurtleGameControl();
  void setBoardSize(int size);
  void start(int boardSize);
  
private:
  
  void drawOptions();
  bool handleOptions(int option, int step);
  TurtleBoardControl tbc;
  int _board_size;
  std::map<std::string, int> options;
   typedef  std::map<std::string, int>::iterator it_type;
   
};

#endif