#ifndef TURTLE_BOARD_CONTROL_H
#define TURTLE_BOARD_CONTROL_H

#include "point.h"
#include "turtle.h"
#include "board.h"

class TurtleBoardControl{
public:
  TurtleBoardControl();
  virtual ~TurtleBoardControl();
  void moveTurtle(int step);
  void turnLeftTurtle();
  void turnRightTurtle();
  void turtleWrite(bool status);
  void setBoardSize(int size);
  void print();
private:
  void writeMove(int step);
  void noWriteMove(int step);
  void checkOutOfBoard(Point turtlePosition);
  
  Turtle* _turtle;
  Board *_board;
 
};
#endif