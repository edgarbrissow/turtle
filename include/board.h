#ifndef BOARD_H
#define BOARD_H

class Board{
  
public:
  Board(unsigned size);
  virtual ~Board();
  void write(unsigned row, unsigned col);
  void print();
  
private:
  unsigned _size;
  char** _board;
    
};
#endif