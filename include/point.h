#ifndef POINT_H
#define POINT_H

class Point{
  
public:
  Point();
  virtual ~Point();
  Point(int x, int y);
  int getX();
  int getY();
  void setX(int x);
  void setY(int y);
  void setPoint(int x, int y);
  Point operator +(Point& point);
  Point operator *(Point& point);
  void print();
  void print(char * msg);
private:
  int _x;
  int _y;
};

#endif