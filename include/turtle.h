#ifndef TURTLE_H
#define TURTLE_H

#include "point.h"

class Turtle{
public:
  Turtle(Point initialPosition);
  virtual ~Turtle();
  Point getPosition();
  void setX(int x);
  void setY(int y);
  Point getDirection();
  void turnRight();
  void turnLeft();
  void move(int step);
  bool pen();
  void pen(bool status);
  
private:
  Point _position;
  Point _direction;
  bool _pen;
  
  
  
};
#endif