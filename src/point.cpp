#include "point.h"
#include <iostream>

Point::Point(int x, int y)
:_x(x),_y(y){}

Point::Point(){
  _x = _y = 0;
}

int Point::getX(){
  return _x;
}

int Point::getY(){
  return _y;
}

void Point::setX(int x){
    _x = x;
}

void Point::setY(int y){
  _y = y;
}

void Point::setPoint(int x, int y){
   _x = x;
   _y = y;
}

Point Point::operator*(Point& point){
  return Point(
    _x * point._x,
    _y * point._y
  );
}

Point Point::operator+(Point& point){
  return Point(
    _x + point._x,
    _y + point._y
  );
}

void Point::print()
{
  std::cout << "POINT X: " << _x << " - PONT Y: " << _y << std::endl;
}

void Point::print(char* msg)
{
     std::cout << msg<<  " :: POINT X: " << _x << " - PONT Y: " << _y << std::endl;
}


Point::~Point()
{

}
