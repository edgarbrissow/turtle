#include "turtle_game_control.h"
#include <iostream>
#include <sstream>

TurtleGameControl::TurtleGameControl(){
    options["END"] = 0;
    options["PEN_UP"] = 1;
    options["PEN_DOWN"] = 2;
    options["LEFT"] = 3;
    options["RIGHT"] = 4;
    options["WALK"] = 5;
    options["PRINT"] = 6;
    tbc.setBoardSize(20);
}

TurtleGameControl::~TurtleGameControl(){}

void TurtleGameControl::start(int boardSize){
  drawOptions();
  std::string s;
  int comand;
  char comma;
  int op = 0;
  while(true){
    
    comma = 'f';
    comand = 0;
    std:: cout << "Digite a opção: ";
    std::cin >> s;
      std::stringstream ss(s);
      ss >> op >> comma >> comand;
      if(handleOptions(op, comand))
	break;    
      if(op == 6)
	drawOptions();
  }

}
void TurtleGameControl::drawOptions(){
  std::cout << "\t\t\tOPTIONS:" << std::endl << std::endl;
  for(it_type iterator =options.begin(); iterator != options.end(); iterator++){
    std::cout << iterator->first << " : " << iterator->second << std::endl;
  }
  std::cout << std::endl << std::endl;
}

bool TurtleGameControl::handleOptions(int option, int step){
  switch(option){
    case 0:
      return true;
      break;
      
    case 1:
      tbc.turtleWrite(false);
      break;
      
    case 2:
      tbc.turtleWrite(true);
      break;    
      
    case 3:
      tbc.turnLeftTurtle();
      break;
      
    case 4:
      tbc.turnRightTurtle();
      break;
      
    case 5:
      tbc.moveTurtle(step);
      break;
     
    case 6:
      tbc.print();
      break;
      
    default:
      break;
      
  }
  return false;
}



void TurtleGameControl::setBoardSize(int size){}


