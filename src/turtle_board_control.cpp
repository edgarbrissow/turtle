#include "turtle_board_control.h"
#include <vector>

TurtleBoardControl::TurtleBoardControl(){
  _turtle = new Turtle(Point(0,0));
}

TurtleBoardControl::~TurtleBoardControl(){}

void TurtleBoardControl::checkOutOfBoard(Point turtlePosition){}

void TurtleBoardControl::moveTurtle(int step){
  if(_turtle->pen())
    writeMove(step);
  else
    noWriteMove(step);
}

void TurtleBoardControl::noWriteMove(int step){
    _turtle->move(step);
}

void TurtleBoardControl::print(){
    _board->print();
}

void TurtleBoardControl::turnLeftTurtle(){
  _turtle->turnLeft();
}

void TurtleBoardControl::turnRightTurtle(){
  _turtle->turnRight();
}

void TurtleBoardControl::turtleWrite(bool status){
  _turtle->pen(status);
}

void TurtleBoardControl::writeMove(int step){
     int x = _turtle->getPosition().getX();
     int y = _turtle->getPosition().getY();
     int j = 0;
     int index=0;
    _turtle->move(step);
    std::vector<Point> list;
    for(int i=x;  i < _turtle->getPosition().getX(); i++, j++){
      Point p(x+j, y);
      list.push_back(p);
    }
    
    for(int i=x;  i > _turtle->getPosition().getX(); i--, j--){
      Point p(x+j, y);
      list.push_back(p);
    }
  
   for(int i=y;  i > _turtle->getPosition().getY(); i--, j--){
          Point p(x, y+j);
      list.push_back(p);
    }
    
   for(int i=y;  i < _turtle->getPosition().getY(); i++, j++){
      Point p(x, y+j);
      list.push_back(p);
    }
    
    for (int i=0; i < list.size();i++ ){
      _board->write(list.at(i).getX(), list.at(i).getY());
    }  
}

void TurtleBoardControl::setBoardSize(int size){
  _board = new Board(size);
}
