#include "board.h"
#include <iostream>

Board::Board(unsigned int size)
{
  _size = size;
  _board = new char*[size];
  for(int i=0; i< _size; i++){
    for(int j=0; j< _size; j++){
      _board[i] = new char [_size];
    }
  }
  
  for(int i=0; i< _size; i++){
    for(int j=0; j< _size; j++){
      if(j ==0 || j== _size-1)
	 _board[i][j] = '|';
      else
	if(i == 0 || i == _size-1)
	  _board[i][j] = '-';
	else
	  _board[i][j] = ' ';
    }
  }
}
//y+x*linha
void Board::print()
{
//   _board[3][3] = '';
    for(int j=0; j< _size; j++){
	std::cout << _board[j] << std::endl;
    }
  
}

void Board::write(unsigned int row, unsigned int col)
{
  ++row;
  ++col;
  if(row < _size-1 && col < _size-1 )
    _board[col][row] = '~';
}

Board::~Board()
{
  for(int i=0; i<_size; i++){
    delete _board[i];
  }
}
