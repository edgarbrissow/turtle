#include "turtle.h"

Turtle::Turtle(Point initialPosition)
:_position(initialPosition){
  _direction.setPoint(1, 0);
}

void Turtle::setX(int x){
  _position.setX(x);
}

void Turtle::setY(int y){
  _position.setY(y);
}

Point Turtle::getDirection(){
  return _direction;
}

Point Turtle::getPosition(){
  return _position;
}

void Turtle::move(int step){
  Point p(step, step);
  
//   p.print();
  
//   _direction.print();
  Point move = p * _direction;
  
//   move.print();
  
  _position = _position + move;
}

bool Turtle::pen()
{
  return _pen;
}

void Turtle::pen(bool status)
{
  _pen = status;
}

void Turtle::turnRight()
{
  
  int x =  _direction.getX() * 0 + _direction.getY() * -1;
  int y = _direction.getX() * 1 + _direction.getY() * 0  ;
  
  _direction.setPoint(x, y);
}

void Turtle::turnLeft()
{
  int x =  _direction.getX() * 0 + _direction.getY() * 1;
  int y = _direction.getX() * -1 + _direction.getY() * 0  ;
  
  _direction.setPoint(x, y);
  
}

Turtle::~Turtle()
{
  _position.setPoint(0,0);;
}





